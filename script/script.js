"use strict";

/*
*@desc when the DOM is loaded, calls the setup function
 */
window.addEventListener("load", setup);

/*
*@desc creates global valuable and array
 */
let todoarr = [];
let btn;

let formRange;
let tasks = 0;

/*
 *@desc calls loadTodo function to load the local storage, adds event for important value's change and submit button  
 */
function setup() {
    loadTodo();
    formRange = document.querySelector(".item4");
    formRange.addEventListener("click", changeRangeValue);

    btn = document.querySelector("form");
    btn.addEventListener("submit", submit);
}

/*
 *@desc creates a task object and pushes it to the global array, calls displayTask function and prevent default reload
  * @param {*} e 
 */
function submit(e) {
    let name = document.querySelector(".item1").value;
    let desc = document.querySelector(".item2").value;
    let cate;
    let categories = document.querySelectorAll(".item3");
    for(let i = 0; i < categories.length; i++) {
        if(categories[i].checked) {
            cate = categories[i].value;
            break;
        }
    }
    let impo = document.querySelector(".item4").value;
    
    let taskObj = {
        id: tasks,
        name: name, 
        desc: desc,
        cate: cate,
        impo: impo,
    } 
    tasks++;

    todoarr.push(taskObj);
    //
    localStorage.setItem('store',JSON.stringify(todoarr));
    //
    displayTask(taskObj);
    btn.reset();
    

    e.preventDefault();
}

/*
 *@desc display the list of tasks by adding a String into html file 
 * @param {*} taskObj 
 */
function displayTask(taskObj) {
    let taskBlock = 
    `<div id="tasks${taskObj.id}" class="container2">
    <h2 class="taskname"></h2>
    <hr> 
        <div class="column">
            <label class="delete">Description</label>
            <p class="taskdesc"></p>
        </div>
        <div class="column">
            <label class="delete">Category</label> 
            <p class="taskcate"></p>
        </div>
        
        <div class="column">
            <label id="label">Importance</label>
            <p class="importantvaluelist"></p>
        </div>
        
        <div class="delete">
            <button id="btn${taskObj.id}" class="deletebtn">Finish</button>
        </div>
    </div>`
    ;

    document.querySelector("#form").insertAdjacentHTML("afterend", taskBlock);

    document.querySelector(".taskname").innerText = taskObj.name;
    document.querySelector(".taskdesc").innerText = taskObj.desc;
    document.querySelector(".taskcate").innerText = taskObj.cate;

    document.querySelector(".importantvaluelist").innerText = taskObj.impo;

    document.getElementById(`btn${taskObj.id}`).addEventListener("click", checkedAction);

}

/*
 *@desc change the display of the important value whenever the range input is moved 
 */
function changeRangeValue() {
    document.querySelector(".importantvalue").innerText = formRange.value;
}

/*
 *@desc when a task is checked as finished, the task object will be remove from the array and it will no longer be displayed on the DOM 
 * @param {*} event 
 */
function checkedAction(event){
    event.currentTarget.parentElement.parentElement.remove();
    let finished = event.currentTarget.parentElement.parentElement.getAttribute("id");
    for(let i=0; i<todoarr.length;i++){
        if(finished == `tasks${todoarr[i].id}`){
            todoarr.splice(i, 1);
            localStorage.setItem('store',JSON.stringify(todoarr));
            break;
        }
        
    }
}

/*
 *@desc store the tasks into the local storage 
 */
function loadTodo(){
    if(localStorage.getItem('store')){
        todoarr=JSON.parse(localStorage.getItem('store'));
        for(let i=0; i<todoarr.length; i++){
            displayTask(todoarr[i]);
        }
    }
}
